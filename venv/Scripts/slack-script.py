#!C:\Users\student\PycharmProjects\local_test\venv\Scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'slack-webhook-cli==0.2.4','console_scripts','slack'
__requires__ = 'slack-webhook-cli==0.2.4'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('slack-webhook-cli==0.2.4', 'console_scripts', 'slack')()
    )
